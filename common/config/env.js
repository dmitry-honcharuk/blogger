const { CLIENT_PORT, SERVER_PORT } = process.env;

module.exports = {
  CLIENT_PORT,
  SERVER_PORT,
};
