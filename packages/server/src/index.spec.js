import request from 'supertest';
import { establishConnection, server } from '../tests/helpers/withServer';

describe('Server app', () => {
  establishConnection();

  it('should answer with status 200 on root route', done =>
    request(server)
      .get('/')
      .expect('Content-Type', /html/)
      .expect(200)
      .end(err => {
        if (err) {
          throw err;
        }
        done();
      }));
});
