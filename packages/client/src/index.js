import express from 'express';

import { CLIENT_PORT as PORT } from '@blogger/config';

const app = express();

app.get('/', (req, res) => res.send('<h1>Hello there, Client</h1>'));

app.listen(PORT, () => console.info(`Listening on port ${PORT}`));
