import express from 'express';

import { SERVER_PORT as PORT } from '@blogger/config';

export const app = express();

app.get('/', (req, res) => res.send('<h1>Hello there, Server</h1>'));

export const startServer = () =>
  app.listen(PORT, () => console.info(`Listening on port ${PORT}`));

if (process.env.NODE_ENV !== 'test') {
  startServer();
}
