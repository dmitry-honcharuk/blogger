const path = require('path');
const merge = require('webpack-merge');
const config = require('@blogger/config/webpack/webpack.dev.config');

module.exports = merge(config, {
  entry: [path.resolve(__dirname, '../src', 'index.js')],
});
