import { app, startServer } from '../../src/index';

let connection;

export const server = app;

export const setupConnection = async () => {
  connection = await startServer();
};

export const closeConnection = () => {
  connection.close();
};

export const establishConnection = () => {
  beforeAll(setupConnection);
  afterAll(closeConnection);
};
