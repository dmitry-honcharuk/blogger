const path = require('path');

require('dotenv-extended').load({
  errorOnMissing: true,
  errorOnExtra: true,
  path: path.resolve(__dirname, '../../.env'),
  defaults: path.resolve(__dirname, '../../.env.defaults'),
  schema: path.resolve(__dirname, '../../.env.schema'),
});
