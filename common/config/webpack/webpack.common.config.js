const path = require('path');
const nodeExternals = require('webpack-node-externals');
const NodemonPlugin = require('nodemon-webpack-plugin');

const { appPath } = require('../paths');

module.exports = {
  mode: 'development',
  devtool: 'eval',
  entry: [path.resolve(__dirname, '../setup-env.js')],
  output: {
    path: path.resolve(appPath, 'dist'),
    filename: 'bundle.js',
  },
  target: 'node',
  externals: [
    nodeExternals({
      modulesFromFile: true,
    }),
  ],
  node: {
    __dirname: true,
    __filename: true,
  },
  stats: 'verbose',
  plugins: [new NodemonPlugin()],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                '@babel/preset-env',
                {
                  targets: {
                    node: true,
                  },
                  modules: 'commonjs',
                },
              ],
            ],
          },
        },
      },
    ],
  },
};
